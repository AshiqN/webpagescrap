const gulp = require('gulp');



const { src, dest, series } = require('gulp');
const fs = require('fs');
const install = require('gulp-install');
const zip = require('gulp-zip');
const path = require('path');

const config = {
    projectFolder: './src',
    destinationFolder: './bundle'
};

function changePackageFile(cb) {
    const packageFilePath = path.join(process.cwd(), config.destinationFolder, 'package.json');
    const tsConfig = require(packageFilePath);
    tsConfig.main = './index.js';

    fs.writeFileSync(packageFilePath, JSON.stringify(tsConfig, null, 2));
    cb()
}

function prepareProject() {
    return src([
        config.projectFolder + '/**/*',
        'package.json',
    ])
        .pipe(dest(config.destinationFolder))
        .pipe(install({
            npm: '--production',
        }));
}


function build() {
    return src(config.destinationFolder + '/**/*', { nodir: true })
        .pipe(zip('bundle.zip'))
        .pipe(dest('.'));
}


exports.default = series(
    prepareProject,
    changePackageFile,
    build,
);
exports.build = exports.default;