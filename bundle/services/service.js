
var Meta = require('html-metadata-parser');
const ogs = require('open-graph-scraper');

// const webpageScrapping =async (reqBody) => {

//     //scrapping the page metadata
//     var result = await Meta.parser(reqBody.url)
//     return result
    
// }

const webpageScrapping =async (reqBody) => {

    const options = { url: reqBody.url };
    // Do the external Call
    let data
    try {
        data=await ogs(options)
       
    } catch (err) {
        if(err.error){
            return {status: 400, data: "Invalid Request"}
        }else{
            return {status: 500, data: "Internal Server Error"}
        }
        
    }
    return {status: 200, data: data.result}
}

module.exports = {
    webpageScrapping,
};