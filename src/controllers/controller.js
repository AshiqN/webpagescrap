

const service = require("../services/service");

const webpageScrapping= async(req, res) => {
    const { body } = req;
    if (!body.url) {
      return;
    }
    //request body
    const reqBody = {
      url: body.url,
    };
    const metaData = await service.webpageScrapping(reqBody);
    if(metaData.status!=200){
      res.status(metaData.status).send({ data: metaData });
    }else{
      res.status(200).send({ data: metaData });
    }
    
    
};

module.exports = {
    webpageScrapping,
};