const express = require("express"); 
const v1Route = require("./v1/routes/routes");
const bodyParser = require("body-parser");

const app = express(); 
const PORT = process.env.PORT || 3000; 
const HOST=process.env.HOST || 'localhost';

//parse the sent JSON inside the request body
app.use(bodyParser.json());

//versioning application
app.use("/v1", v1Route);

app.listen(PORT,HOST, () => {
  console.log(`API is listening on host ${HOST} port ${PORT}`);
});
 
