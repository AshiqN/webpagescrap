const express = require("express");
const controller = require("../../controllers/controller");
const router = express.Router();

//route handling
router.post("/web/metadata", controller.webpageScrapping);

module.exports = router;